const lastname = document.getElementById('lastname');
const firstname = document.getElementById('firstname');
const birthdate = document.getElementById('birthdate');
const username = document.getElementById('username');
const pwd = document.getElementById('userpwd');
const mail = document.getElementById('useremail');
const btn = document.getElementById('submit');
const errorElement = document.getElementById('error');
const formulaire = document.getElementById('inscription-form');
const reglepwd = /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\-;:!?./*&$]).{12,}/;
// Regex de validation d'adresse mail recommandée par W3C
const regleadressemail = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

formulaire.addEventListener('submit', (e) => {
    
    let saisievalide = true;
    let messages = [];
    let datesaisies = birthdate.value.split('/');

    console.log("date : " + datesaisies);  
    console.log("nom : "+lastname.value);
    console.log("prenom : "+firstname.value);
    console.log("pseudo : "+username.value);
    console.log("mdp : "+pwd.value);  
    console.log("mail : "+mail.value);  
    
    

    if(lastname.value == ""){
        saisievalide = false;
        messages.push("Vous avez un nom");
    }
    console.log("saisie valide ?" + saisievalide); 

    if(firstname.value == ""){
        saisievalide = false;
        messages.push("Vous avez un prénom");
    }
    console.log("saisie valide ?" + saisievalide);

    if (username.value.length < 6) {
        saisievalide = false;
        messages.push("Le nom d'utilisateur doit faire au moins 6 caractères");
    }
    console.log("saisie valide username?" + saisievalide);  

    // TODO mettre un switch ?
    if((birthdate.value == "") || (datesaisies.length == 3)){
        

        let dateconstruite;
        if(datesaisies.length == 3){
            dateconstruite = new Date(datesaisies[2],(datesaisies[1]-1),datesaisies[0]);
            console.log(dateconstruite);
            if(dateconstruite.toLocaleDateString("fr-FR") != birthdate.value){
                saisievalide = false;
                messages.push("La date saisie n'est pas valide");
            }
        }
        else{
            console.log("je passe la !")

            dateconstruite = new Date();
        }
    }
    else{
        saisievalide = false;
        messages.push("La date saisie n'est pas valide")
    }
    console.log("saisie valide date ?" + saisievalide);  

    if (!reglepwd.test(pwd.value)) {
        saisievalide = false;
        messages.push("Le mot de passe n'est pas valide (au moins 12 caractères dont une majuscule, une minuscule, un chiffre et l'un des caractères spéciaux suivants (_-;:!?./*&$) )")
    }


    console.log("saisie valide ?" + saisievalide); 
    if (!regleadressemail.test(mail.value)) {
        saisievalide = false;
        messages.push("L'adresse mail n'est pas valide");
    }
    
    
    console.log("saisie valide ?" + saisievalide);  
    if(saisievalide){
        console.log("formulaire correct");

    }
    else{
        e.preventDefault();
        errorElement.innerText = "Formulaire incorrect : \n"+messages.join(', \n');
        errorElement.style.outline = "3px solid red";
    }

})

