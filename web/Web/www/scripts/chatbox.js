const sendForm = document.getElementById('chat-form');
const msgList = document.getElementById('msg-list');
let xhrGetMsgs = new XMLHttpRequest();
xhrGetMsgs.open('GET', '../htbin/chatget.py');

xhrGetMsgs.responseType = 'json';
xhrGetMsgs.send();

let formatedString = '';
let newLi;
let newLiNode;

function afficherMessage(message){
    formatedString = 'le ' + message.date + ' a ' + message.time + ', ' + message.user + ' : ' + message.msg;
    newLi = document.createElement('li');
    newLiNode = document.createTextNode(formatedString);
    newLi.appendChild(newLiNode);
    msgList.appendChild(newLi);
}

xhrGetMsgs.onload = function() {
    if (xhrGetMsgs.status != 200) {
        console.log('Erreur réponse serveur');
    } else {
        xhrGetMsgs.response.forEach(message => {
            afficherMessage(message);
        });
    }
}


// Logique pour envoyer des messages
let messageEcrit = document.getElementById("chat-msg-input")
let FormMessage = new FormData();
let xhrPostMsg = new XMLHttpRequest();
let msgErreur = document.getElementById("error");
let xhrGetLastPost = new XMLHttpRequest();
let lastPost
xhrPostMsg.responseType = 'json';





sendForm.addEventListener('submit', function(event){ 
    if(messageEcrit.value != ''){
        xhrPostMsg.open('POST', '../htbin/chatsend.py');
        FormMessage.set("msg",messageEcrit.value);
        xhrPostMsg.send(FormMessage);
        msgErreur.innerText = "";
    }
    else{
        msgErreur.innerText = "Le message ne peut être vide !"
    }
    event.preventDefault();
});

xhrPostMsg.onload = function() {
    if (xhrPostMsg.status != 200) {
        console.log('error while sending new chat message.');
    } else {
        console.log(xhrPostMsg);
        switch (xhrPostMsg.response.num) {
            case -1:
                msgErreur.innerText = "Erreur ! pas d'username !"
                break;
            case 0:
                xhrGetLastPost.open('GET', '../htbin/chatget.py', false);
                xhrGetLastPost.send();

                // on recupère uniquement le json correspondant au dernier message de la "Base" des messages
                lastPost = JSON.parse(xhrGetLastPost.response).slice(-1)[0];
                console.log(lastPost);
                afficherMessage(lastPost);
                break;
            default:
                break;
        }        
    }
}

