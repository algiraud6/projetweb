const username = document.getElementById('username');
const pwd = document.getElementById('userpwd');
const chatBtn = document.getElementById('chat-button');
const formulaire = document.getElementById('login-form');
const response = document.getElementById('response'); // Assuming you have an element with ID 'response' for displaying response
const requete = new XMLHttpRequest();

requete.responseType = 'text';

formulaire.addEventListener("submit", (e) => {
    e.preventDefault();

    let formData = new FormData();
    formData.append("username", username.value);
    formData.append("userpwd", pwd.value);

    requete.open("POST", "./htbin/login.py");
    requete.send(formData);
});

requete.onload = function() {
    if (requete.status === 200) {
        response.innerText = requete.responseText;
        // On regarde que la connexion se soit bien passé
        if(requete.response[0] != "L"){
            console.log("test")
            response.innerText = requete.response;
            chatBtn.hidden = false; // Hide the chat button
        }
        else{
            chatBtn.hidden = true;
        }
    }
    else{
        response.innerText = "Error: " + requete.status;
    } 
};

