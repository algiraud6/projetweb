// Gestion des événements pour les boutons de daltonisme
document.getElementById("tritanopiaButton").addEventListener("click", function() {
    changeColorsForTritanopia();
});

document.getElementById("deuteranopiaButton").addEventListener("click", function() {
    changeColorsForDeuteranopia();
});

document.getElementById("protanopiaButton").addEventListener("click", function() {
    changeColorsForProtanopia();
});

document.getElementById("achromatomalyButton").addEventListener("click", function() {
    changeColorsForAchromatomaly();
});

document.getElementById("monochromacyButton").addEventListener("click", function() {
    changeColorsForMonochromacy();
});

document.getElementById("NormalButton").addEventListener("click", function(){
    normalColor();
});


// Fonctions pour changer les couleurs du site pour différents types de daltonisme
function changeColorsForTritanopia() {
    // Changer les couleurs du fond et du texte du body pour la tritanopie
    document.body.style.backgroundColor = "#D2B48C"; // Fond grey
    document.body.style.color = "#000"; // Texte noir
    // Ajoutez d'autres modifications de couleur pour la tritanopie selon vos préférences
}

function changeColorsForDeuteranopia() {
    // Changer les couleurs du fond et du texte du body pour la deutéranopie
    document.body.style.backgroundColor = "#8a2be2"; // Fond violet
    document.body.style.color = "#000"; // Texte noir
    // Ajoutez d'autres modifications de couleur pour la deutéranopie selon vos préférences
}


function changeColorsForProtanopia() {
    // Changer les couleurs du fond et du texte du body pour la proptanopie
    document.body.style.backgroundColor = "#B0E0E6"; // Fond blanc
    document.body.style.color = "#000"; // Texte noir
    // Ajoutez d'autres modifications de couleur pour la proptanopie selon vos préférences
}

function changeColorsForAchromatomaly() {
    // Changer les couleurs du fond et du texte du body pour l'achromatomalie
    document.body.style.backgroundColor = "#AED6F1"; // Fond blanc
    document.body.style.color = "#000"; // Texte noir
    // Ajoutez d'autres modifications de couleur pour l'achromatomalie selon vos préférences
}

function changeColorsForMonochromacy() {
    // Changer les couleurs du fond et du texte du body pour la monochromie
    document.body.style.backgroundColor = "#666666"; // Fond blanc
    document.body.style.color = "#000"; // Texte noir
    // Ajoutez d'autres modifications de couleur pour la monochromie selon vos préférences
}

function normalColor() {
    document.body.style.backgroundColor = "#fff"; // Fond blanc
    document.body.style.color = "#000"; // Texte noir
}

document.addEventListener("DOMContentLoaded", function() {
    const navItems = document.querySelectorAll('.nav-item');

    navItems.forEach(item => {
        item.addEventListener('click', function() {
            // Ajoutez ici le code pour gérer le clic sur les éléments de navigation
        });
    });
});

