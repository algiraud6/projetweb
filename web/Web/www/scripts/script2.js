// Récupérer le paramètre "part" de l'URL
const urlParams = new URLSearchParams(window.location.search);
const part = urlParams.get('part');

// Si le paramètre "part" existe, masquer toutes les parties sauf celle spécifiée
if (part) {
    const allParts = document.querySelectorAll('.partie-content section');
    allParts.forEach(partSection => {
        if (partSection.id !== 'part' + part) {
            partSection.style.display = 'none';
        }
    });
}